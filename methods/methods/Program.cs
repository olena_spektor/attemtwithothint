﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace methods
{
    class Program
    {
        
        static void Main(string[] args)
        {
            string name = "";
            int age = 0;
            string colour = "";
            int rooms = 0;

            Console.WriteLine(Greeting(name, age));
            Console.WriteLine(Colours(colour, rooms));
        }

        static string Greeting(string name, int age)
        {
            Console.WriteLine("What's your name? ");
            name = Console.ReadLine();

            Console.WriteLine("Your age?");
            age = int.Parse(Console.ReadLine());

            return $"{name}, you are {age} years old\n";
        }

        static string Colours(string colour, int rooms)
        {


            Console.WriteLine("What colour is your house Olena?");
            colour = Console.ReadLine();

            Console.WriteLine("How many rooms does your house have?");
            rooms = int.Parse(Console.ReadLine());

            return $"your house has {rooms} rooms and is the colour of your house is {colour}";

        }
    }
}
